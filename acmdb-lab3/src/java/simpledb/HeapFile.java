package simpledb;

import java.io.*;
import java.util.*;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 * 
 * @see simpledb.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {

	private int fileId;
	private File file;
	private TupleDesc tupleDesc;
	
    /**
     * Constructs a heap file backed by the specified file.
     * 
     * @param f
     *            the file that stores the on-disk backing store for this heap
     *            file.
     */
    public HeapFile(File f, TupleDesc td) {
        // some code goes here
    	this.file = f;
    	this.fileId = f.getAbsoluteFile().hashCode();
    	this.tupleDesc = td;
    }

    /**
     * Returns the File backing this HeapFile on disk.
     * 
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        // some code goes here
    	return file;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     * 
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        // some code goes here
    	return fileId;
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     * 
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        // some code goes here
    	return tupleDesc;
    }

    // see DbFile.java for javadocs
    public Page readPage(PageId pid) {
        // some code goes here
    	int pageSize = Database.getBufferPool().getPageSize();
    	byte[] contents = new byte[pageSize];
    	
    	try {
    		RandomAccessFile ramFile = new RandomAccessFile(file.getAbsolutePath(), "r");
    		assert (pageSize * pid.pageNumber() <= file.length());
    		ramFile.seek(pageSize * pid.pageNumber());
    		ramFile.read(contents);
    		ramFile.close();
    		return new HeapPage((HeapPageId)pid, contents);
    	} catch (IOException e) {
    		System.out.println("IOException: " + e.getMessage());
    		return null;
    	}
    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
        // some code goes here
        // not necessary for lab1
        try {
            byte[] data = page.getPageData();
            RandomAccessFile writtenFile = new RandomAccessFile(file, "rw");
            writtenFile.seek(page.getId().pageNumber() * BufferPool.getPageSize());
            writtenFile.write(data);
            writtenFile.close();
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        // some code goes here
    	return (int)Math.ceil((double)file.length() / Database.getBufferPool().getPageSize());
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
    	ArrayList<Page> pageList = new ArrayList<Page>();
    	BufferPool bufferPool = Database.getBufferPool();
    	for (int i = 0; i < numPages(); i++) {
    		PageId pid = new HeapPageId(getId(), i);
    		HeapPage page = (HeapPage)bufferPool.getPage(tid, pid, Permissions.READ_WRITE);
            if (page.getNumEmptySlots() != 0) {
                page.insertTuple(t);
                pageList.add(page);
                break;
            }
    	}
    	if (pageList.isEmpty()) {
            PageId pid = new HeapPageId(getId(), numPages());
            try {
                byte[] bytes = HeapPage.createEmptyPageData();
                RandomAccessFile file = new RandomAccessFile(getFile(), "rw");
                file.seek(pid.pageNumber() * BufferPool.getPageSize());
                file.write(bytes);
            } catch(FileNotFoundException e) {
                e.printStackTrace();
            }
            HeapPage page = (HeapPage)bufferPool.getPage(tid, pid, Permissions.READ_WRITE);
            page.insertTuple(t);
            pageList.add(page);
        }
        return pageList;
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
    	BufferPool bufferPool = Database.getBufferPool();
        HeapPage page = (HeapPage)bufferPool.getPage(tid, t.getRecordId().getPageId(), Permissions.READ_WRITE);
        page.deleteTuple(t);
        
        ArrayList<Page> resultList = new ArrayList<Page>();
        resultList.add(page);
        return resultList;
    }

    private class HeapFileIterator implements DbFileIterator {
    	private HeapFile heapFile;
    	private TransactionId transactionId;
    	private int pageNo;
    	Iterator<Tuple> tupleIterator;
    	
    	public HeapFileIterator(HeapFile heapFile, TransactionId tid) {
    		this.heapFile = heapFile;
    		this.transactionId = tid;
    		this.pageNo = Integer.MAX_VALUE;
    	}

		@Override
		public void open() throws DbException, TransactionAbortedException {
			pageNo = -1;
			tupleIterator = null;
		}

		@Override
		public boolean hasNext() throws DbException, TransactionAbortedException {
			if (pageNo == Integer.MAX_VALUE) {
				return false;
			} else if (tupleIterator != null && tupleIterator.hasNext()) {
				return true;
			} else {
				int pageNoBac = pageNo;
				Iterator<Tuple> tupleIteratorBac = tupleIterator;
				while (pageNo + 1 < heapFile.numPages()) {
					pageNo += 1;
					HeapPageId heapPageId = new HeapPageId(heapFile.getId(), pageNo);
					HeapPage heapPage = (HeapPage)Database.getBufferPool().getPage(transactionId, heapPageId, Permissions.READ_ONLY);
					tupleIterator = heapPage.iterator();
					if (tupleIterator.hasNext()) {
						pageNo = pageNoBac;
						tupleIterator = tupleIteratorBac;
						return true;
					}
				}
				pageNo = pageNoBac;
				tupleIterator = tupleIteratorBac;
				return false;
			}
		}

		@Override
		public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException {
			if (pageNo == Integer.MAX_VALUE) {
				throw new NoSuchElementException();
			} else if (tupleIterator != null && tupleIterator.hasNext()) {
				return tupleIterator.next();
			} else {
				while (pageNo + 1 < heapFile.numPages()) {
					pageNo += 1;
					HeapPageId heapPageId = new HeapPageId(heapFile.getId(), pageNo);
					HeapPage heapPage = (HeapPage)Database.getBufferPool().getPage(transactionId, heapPageId, Permissions.READ_ONLY);
					tupleIterator = heapPage.iterator();
					if (tupleIterator.hasNext()) {
						return tupleIterator.next();
					}
				}
				throw new NoSuchElementException();
			}
		}

		@Override
		public void rewind() throws DbException, TransactionAbortedException {
			close();
			open();
		}

		@Override
		public void close() {
			tupleIterator = null;
			pageNo = Integer.MAX_VALUE;
		}
    }
    
    // see DbFile.java for javadocs
    public DbFileIterator iterator(TransactionId tid) {
        // some code goes here
    	return new HeapFileIterator(this, tid);
    }
}

