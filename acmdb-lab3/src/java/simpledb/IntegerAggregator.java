package simpledb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Knows how to compute some aggregate over a set of IntFields.
 */
public class IntegerAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;

    private int gbField;
    private Type gbFieldType;
    private int aField;
    private Op what;
    private Map<Field, Integer> map;
    private Map<Field, Integer> countMap, sumMap;
    private TupleDesc tupleDesc;
    
    /**
     * Aggregate constructor
     * 
     * @param gbfield
     *            the 0-based index of the group-by field in the tuple, or
     *            NO_GROUPING if there is no grouping
     * @param gbfieldtype
     *            the type of the group by field (e.g., Type.INT_TYPE), or null
     *            if there is no grouping
     * @param afield
     *            the 0-based index of the aggregate field in the tuple
     * @param what
     *            the aggregation operator
     */
    public IntegerAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        // some code goes here
    	this.gbField = gbfield;
    	this.gbFieldType = gbfieldtype;
    	this.aField = afield;
    	this.what = what;
    	this.map = new HashMap<Field, Integer>();
        this.countMap = new HashMap<Field, Integer>();
        this.sumMap = new HashMap<Field, Integer>();
        this.tupleDesc = null;
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the
     * constructor
     * 
     * @param tup
     *            the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        // some code goes here
    	Field field = null;
    	if (gbField == Aggregator.NO_GROUPING) {
    		field = new IntField(Aggregator.NO_GROUPING);
    	} else {
    		field = tup.getField(gbField);
    	}
    	
    	int value = 0;
    	if (what == Op.MIN) {
    		if (map.get(field) == null) {
    			value = ((IntField)tup.getField(aField)).getValue();
    		} else {
    			value = Math.min(((IntField)tup.getField(aField)).getValue(), map.get(field));
    		}
    	} else if (what == Op.MAX) {
    		if (map.get(field) == null) {
    			value = ((IntField)tup.getField(aField)).getValue();
    		} else {
    			value = Math.max(((IntField)tup.getField(aField)).getValue(), map.get(field));
    		}
    	} else if (what == Op.SUM) {
    		if (map.get(field) == null) {
    			value = ((IntField)tup.getField(aField)).getValue();
    		} else {
    			value = ((IntField)tup.getField(aField)).getValue() + map.get(field);
    		}
    	} else if (what == Op.AVG) {
    		if (countMap.get(field) == null) {
                countMap.put(field, 1);
                value = ((IntField)tup.getField(aField)).getValue();
                sumMap.put(field, value);
            } else {
                value = sumMap.get(field) + ((IntField)tup.getField(aField)).getValue();
                sumMap.put(field, value);
                int count = countMap.get(field) + 1;
                countMap.put(field, count);
                value /= count;
            }
    	} else if (what == Op.COUNT) {
    		if (map.get(field) == null) {
                value = 1;
            } else {
                value = map.get(field) + 1;
            }
    	}
    	map.put(field, value);
        if (tupleDesc == null) {
        	Type[] typeAr;
            String[] fieldAr;
            if (gbField == Aggregator.NO_GROUPING) {
                typeAr = new Type[]{Type.INT_TYPE};
                fieldAr = new String[]{tup.getTupleDesc().getFieldName(aField)};
            } else {
                typeAr = new Type[2];
                fieldAr = new String[2];
                typeAr[0] = tup.getTupleDesc().getFieldType(gbField);
                fieldAr[0] = tup.getTupleDesc().getFieldName(gbField);
                typeAr[1] = Type.INT_TYPE;
                fieldAr[1] = tup.getTupleDesc().getFieldName(aField);
            }
            tupleDesc = new TupleDesc(typeAr, fieldAr);
        }
    }

    /**
     * Create a DbIterator over group aggregate results.
     * 
     * @return a DbIterator whose tuples are the pair (groupVal, aggregateVal)
     *         if using group, or a single (aggregateVal) if no grouping. The
     *         aggregateVal is determined by the type of aggregate specified in
     *         the constructor.
     */
    public DbIterator iterator() {
        // some code goes here
    	ArrayList<Tuple> arrayList = new ArrayList<Tuple>();
    	for (Field field : map.keySet()) {
    		Tuple tuple = new Tuple(tupleDesc);
    		if (gbField == Aggregator.NO_GROUPING) {
    			tuple.setField(0, new IntField(map.get(field)));
    		} else {
    			tuple.setField(0, field);
    			tuple.setField(1, new IntField(map.get(field)));
    		}
    		arrayList.add(tuple);
    	}
    	return new TupleIterator(tupleDesc, arrayList);
    }

}
