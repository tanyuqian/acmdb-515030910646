package simpledb;

import java.util.HashMap;

/** A class to represent a fixed-width histogram over a single integer-based field.
 */
public class IntHistogram {

	
	private int buckets;
	private int min;
	private int max;
	private int width;
	private int size;
	private HashMap<Integer, Integer> map;
    /**
     * Create a new IntHistogram.
     * 
     * This IntHistogram should maintain a histogram of integer values that it receives.
     * It should split the histogram into "buckets" buckets.
     * 
     * The values that are being histogrammed will be provided one-at-a-time through the "addValue()" function.
     * 
     * Your implementation should use space and have execution time that are both
     * constant with respect to the number of values being histogrammed.  For example, you shouldn't 
     * simply store every value that you see in a sorted list.
     * 
     * @param buckets The number of buckets to split the input value into.
     * @param min The minimum integer value that will ever be passed to this class for histogramming
     * @param max The maximum integer value that will ever be passed to this class for histogramming
     */
    public IntHistogram(int buckets, int min, int max) {
    	// some code goes here
    	this.buckets = buckets;
    	this.min = min;
    	this.max = max;
    	
    	width = (max - min) / buckets + 1;
    	size = 0;
    	map = new HashMap<Integer, Integer>();
    	for (int i = 0; i < buckets; i++) {
    		map.put(i, 0);
    	}
    }

    /**
     * Add a value to the set of values that you are keeping a histogram of.
     * @param v Value to add to the histogram
     */
    public void addValue(int v) {
    	// some code goes here
    	int index = (v - min) / this.width;
    	map.put(index, map.get(index) + 1);
    	size += 1;
//    	System.out.println(v);
    }

    /**
     * Estimate the selectivity of a particular predicate and operand on this table.
     * 
     * For example, if "op" is "GREATER_THAN" and "v" is 5, 
     * return your estimate of the fraction of elements that are greater than 5.
     * 
     * @param op Operator
     * @param v Value
     * @return Predicted selectivity of this particular operator and value
     */
    public double estimateSelectivity(Predicate.Op op, int v) {
    	// some code goes here
    	if (v < min) {
    		if (op.equals(Predicate.Op.GREATER_THAN) || op.equals(Predicate.Op.GREATER_THAN_OR_EQ) || op.equals(Predicate.Op.NOT_EQUALS)) {
                return 1.0;
            } else {
            	return 0.0;
            }
    	} else if (v > max) {
    		if (op.equals(Predicate.Op.LESS_THAN) || op.equals(Predicate.Op.LESS_THAN_OR_EQ) || op.equals(Predicate.Op.NOT_EQUALS)) {
                return 1.0;
            } else {
                return 0.0;
            }
    	} else {
    		int index = (v - min) / width;
    		int innerIndex = (v - min) - index * width;
    		int numLegal = 0;
    		
    		if (op.equals(Predicate.Op.EQUALS)) {
    			numLegal += map.get(index) / width;
    		} else if (op.equals(Predicate.Op.GREATER_THAN)) {
    			for (int i = index + 1; i < buckets; i++) {
    				numLegal += map.get(i);
    			}
    			numLegal += (width - 1 - innerIndex) * map.get(index) / width; 
    		} else if (op.equals(Predicate.Op.GREATER_THAN_OR_EQ)) {
    			for (int i = index + 1; i < buckets; i++) {
    				numLegal += map.get(i);
    			}
    			numLegal += (width - innerIndex) * map.get(index) / width;
    		} else if (op.equals(Predicate.Op.LESS_THAN)) {
    			for (int i = 0; i < index; i++) {
    				numLegal += map.get(i);
    			}
    			numLegal += innerIndex * map.get(index) / width;
    		} else if (op.equals(Predicate.Op.LESS_THAN_OR_EQ)) {
    			for (int i = 0; i < index; i++) {
    				numLegal += map.get(i);
    			}
    			numLegal += (innerIndex + 1) * map.get(index) / width;
    		} else if (op.equals(Predicate.Op.NOT_EQUALS)) {
    			numLegal += size - map.get(index) / width;
    		}
    		
    		if (size == 0) {
    			return 0.0;
    		} else {
    			return (double)numLegal / size;
    		}
    	}
    }
    
    /**
     * @return
     *     the average selectivity of this histogram.
     *     
     *     This is not an indispensable method to implement the basic
     *     join optimization. It may be needed if you want to
     *     implement a more efficient optimization
     * */
    public double avgSelectivity()
    {
        // some code goes here
        return 1.0;
    }
    
    /**
     * @return A string describing this histogram, for debugging purposes
     */
    public String toString() {
        // some code goes here
    	return map.toString();
    }
}

