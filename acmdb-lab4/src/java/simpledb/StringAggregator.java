package simpledb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Knows how to compute some aggregate over a set of StringFields.
 */
public class StringAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;

    private int gbField;
    private Type gbFieldType;
    private int aField;
    private Op what;
    private Map<Field, Integer> countMap;
    private TupleDesc tupleDesc;
    
    /**
     * Aggregate constructor
     * @param gbfield the 0-based index of the group-by field in the tuple, or NO_GROUPING if there is no grouping
     * @param gbfieldtype the type of the group by field (e.g., Type.INT_TYPE), or null if there is no grouping
     * @param afield the 0-based index of the aggregate field in the tuple
     * @param what aggregation operator to use -- only supports COUNT
     * @throws IllegalArgumentException if what != COUNT
     */

    public StringAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        // some code goes here
    	this.gbField = gbfield;
    	this.gbFieldType = gbfieldtype;
    	this.aField = afield;
    	this.what = what;
    	this.countMap = new HashMap<Field, Integer>();
    	if (what != Op.COUNT) {
    		throw new IllegalArgumentException("StringAggregator: what must be COUNT.");
    	}
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the constructor
     * @param tup the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        // some code goes here
    	Field field;
        if (gbField == Aggregator.NO_GROUPING) {
            field = new IntField(Aggregator.NO_GROUPING);
        } else {
            field = tup.getField(gbField);
        }
        
        int value = 0;
        if (!countMap.containsKey(field)) {
            value = 1;
        } else {
            value = countMap.get(field) + 1;
        }
        countMap.put(field, value);
        
        if (tupleDesc == null) {
        	Type[] typeAr;
            String[] fieldAr;
            if (gbField == Aggregator.NO_GROUPING) {
                typeAr = new Type[1];
                fieldAr = new String[1];
                typeAr[0] = Type.INT_TYPE;
                fieldAr[0] = tup.getTupleDesc().getFieldName(aField);
            } else {
                typeAr = new Type[2];
                fieldAr = new String[2];
                typeAr[0] = tup.getTupleDesc().getFieldType(gbField);
                typeAr[1] = Type.INT_TYPE;
                fieldAr[0] = tup.getTupleDesc().getFieldName(gbField);
                fieldAr[1] = tup.getTupleDesc().getFieldName(aField);
            }
            tupleDesc = new TupleDesc(typeAr, fieldAr);
        }
    }

    /**
     * Create a DbIterator over group aggregate results.
     *
     * @return a DbIterator whose tuples are the pair (groupVal,
     *   aggregateVal) if using group, or a single (aggregateVal) if no
     *   grouping. The aggregateVal is determined by the type of
     *   aggregate specified in the constructor.
     */
    public DbIterator iterator() {
        // some code goes here
    	ArrayList<Tuple> arrayList = new ArrayList<Tuple>();
        for (Field field : countMap.keySet()) {
            Tuple tuple = new Tuple(tupleDesc);
            if (((IntField)field).getValue() == Aggregator.NO_GROUPING) {
                tuple.setField(0, new IntField(countMap.get(field)));
            } else {
                tuple.setField(0, field);
                tuple.setField(1, new IntField(countMap.get(field)));
            }
            arrayList.add(tuple);
        }
        return new TupleIterator(tupleDesc, arrayList);
    }
}
